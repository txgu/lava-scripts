#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

AFL_HOME=${AFL_HOME:-${DIR}/../afl-2.52b}
AFL_OPTS=${AFL_OPTS:-"-d"}
FUZZER=${FUZZER:-afl}
LAVA_HOME=${DIR}/subjects/lava_corpus

TOOL=${TOOL:-uniq}
#PREFIX=${PREFIX:-${DIR}/${FUZZER}-lava-bin/${TOOL}}
PREFIX=${PREFIX:-${DIR}/qsym-lava-bin/${TOOL}}
TOOL_PATH=${PREFIX}/bin/${TOOL}

INPUT_DIR=${LAVA_HOME}/LAVA-M/${TOOL}/fuzzer_input
OUTPUT_DIR=${DIR}/${FUZZER}-output-${TOOL}

REF_DIR=${LAVA_HOME}/LAVA-M/${TOOL}/inputs
REF_LIST=${DIR}/${FUZZER}-${TOOL}-ref.log
cat ${LAVA_HOME}/LAVA-M/${TOOL}/validated_bugs | sort -u > $REF_LIST

VAL_LIST=${DIR}/${FUZZER}-${TOOL}-output.log
REF_VAL_LIST=${DIR}/${FUZZER}-${TOOL}-ref-output.log

SCRIPT_LOG=${DIR}/${FUZZER}-${TOOL}-script

START_TS=$(cat ${OUTPUT_DIR}/fuzzer_stats | grep start_time | awk '{ print $3}')
printf "$START_TS\n" > $VAL_LIST

for INPUT in $(find $OUTPUT_DIR -path "*/crashes/id*" | sort )
do
    echo "Checking $INPUT"
    script -c "/usr/bin/timeout -s 9 1s ${TOOL_PATH} ${OPTS} ${INPUT} ; exit" $SCRIPT_LOG > /dev/null
    BUG_ID=$(cat $SCRIPT_LOG| tr -cd '[:print:]' | grep -oh 'Successfully triggered bug \([0-9]\+\)' |  sed -n -e 's/.*Successfully triggered bug \([0-9]\+\).*/\1/p'| tail -n 1)
    if [[ $BUG_ID == "" ]];
    then
        echo "No bug ID in $INPUT"
        continue
    fi
    BUG_TS=$(date -r $INPUT +%s)
    printf "$BUG_ID\t$BUG_TS\t`basename $INPUT`\n" >> $VAL_LIST
done
cat $VAL_LIST
