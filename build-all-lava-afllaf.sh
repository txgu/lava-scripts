#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

export FUZZER=afllaf
export AFL_HOME=${HOME}/Projects/afllaf
export CC=${AFL_HOME}/afl-clang-fast
export CXX=${AFL_HOME}/afl-clang-fast++

export LAF_SPLIT_SWITCHES=1
export LAF_TRANSFORM_COMPARES=1
export LAF_SPLIT_COMPARES=1

# Optimization may remove some bugs
export AFL_DONT_OPTIMIZE=1
export CFLAGS="-g -finline-functions -funroll-loops"

TOOL=uniq       bash    ${DIR}/build-lava.sh
TOOL=base64     bash    ${DIR}/build-lava.sh
TOOL=who        bash    ${DIR}/build-lava.sh
TOOL=md5sum     bash    ${DIR}/build-lava.sh
