# Install



## Download

Clone the project into `${HOME}/Projects/lava-scripts`.
The set of scripts requires afl and qsym to be installed at specific locations too.
If you already installed afl and qsym at other places, you can create a soft link for each.

```
AFL_HOME=${HOME}/Projects/afl-2.52b
QSYM_HOME=${HOME}/Projects/qsym
```

## Build

You need to first install clang and build the llvm mode of AFL, and then run `build-all-lava.sh`.

```
bash build-all-lava.sh
```

## Run

Run `uniq` (no opts).

```
# Start afl-master
TOOL=uniq OPTS=  AFL_OPTS="-M afl-master" bash run-lava.sh
# Start afl-slave
TOOL=uniq OPTS=  AFL_OPTS="-S afl-slave"  bash run-lava.sh
# Start qsym
TOOL=uniq OPTS=                           bash run-lava-qsym.sh
```

Run `base64`.

```
TOOL=base64 OPTS=-d  AFL_OPTS="-M afl-master" bash run-lava.sh
TOOL=base64 OPTS=-d  AFL_OPTS="-S afl-slave"  bash run-lava.sh
TOOL=base64 OPTS=-d                           bash run-lava-qsym.sh
```
