#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)


FUZZER=${FUZZER:-afl}
TOOL=${TOOL:-uniq}
PREFIX=${PREFIX:-${DIR}/${FUZZER}-lava-bin/${TOOL}}
LAVA_HOME=${DIR}/subjects/lava_corpus

if [[ ! -d $LAVA_HOME ]]
then
    mkdir ${DIR}/subjects
    pushd ${DIR}/subjects
    wget http://panda.moyix.net/~moyix/lava_corpus.tar.xz
    tar -xvf lava_corpus.tar.xz
    popd
fi

pushd ${LAVA_HOME}/LAVA-M/${TOOL}/coreutils-8.24-lava-safe
#./configure --prefix=$PREFIX  LIBS="-lacl"
./configure --prefix=$PREFIX
make clean
make -j8
make install
popd

