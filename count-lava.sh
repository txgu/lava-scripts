#! /bin/bash

LAVA_LOG=${1:-lava.log}
#cat $LAVA_LOG | tr -cd '[:print:]' | sed -n -e 's/.*Successfully triggered bug \([0-9]\+\).*/\1/pg'
cat $LAVA_LOG | tr -cd '[:print:]' | grep -oh 'Successfully triggered bug \([0-9]\+\)' |  sed -n -e 's/.*Successfully triggered bug \([0-9]\+\).*/\1/p'| tail -n 1 | sort -u
