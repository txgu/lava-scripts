#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

AFL_HOME=${AFL_HOME:-${DIR}/../afl-2.52b}
AFL_OPTS=${AFL_OPTS:-"-d"}
FUZZER=${FUZZER:-afl}
LAVA_HOME=${DIR}/subjects/lava_corpus

TOOL=${TOOL:-uniq}
PREFIX=${PREFIX:-${DIR}/${FUZZER}-lava-bin/${TOOL}}
TOOL_PATH=${PREFIX}/bin/${TOOL}

INPUT_DIR=${LAVA_HOME}/LAVA-M/${TOOL}/fuzzer_input
OUTPUT_DIR=${DIR}/${FUZZER}-output-${TOOL}

${AFL_HOME}/afl-fuzz $AFL_OPTS -i ${INPUT_DIR} -o ${OUTPUT_DIR} -- ${TOOL_PATH} $OPTS @@
