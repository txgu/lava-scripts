#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)


FUZZER=${FUZZER:-afl}
QSYM_HOME=${DIR}/../qsym
LAVA_HOME=${DIR}/subjects/lava_corpus

TOOL=${TOOL:-uniq}
TOOL_PATH=${DIR}/qsym-lava-bin/${TOOL}/bin/${TOOL}

INPUT_DIR=${LAVA_HOME}/LAVA-M/${TOOL}/fuzzer_input
OUTPUT_DIR=${DIR}/${FUZZER}-output-$TOOL

if [[ ! -d $OUTPUT_DIR ]]
then
    echo "Please start AFL first."
    exit
fi

${QSYM_HOME}/bin/run_qsym_afl.py -a afl-slave -n qsym -o ${OUTPUT_DIR} -- ${TOOL_PATH} $OPTS @@
