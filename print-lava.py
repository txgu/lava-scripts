#! /usr/bin/env python2

import os, sys, traceback, subprocess, re, time
from collections import OrderedDict


def get_crashes(all_bugs, output):
    crashes_map = dict()
    unlisted = set()
    with open(output) as f:
        lines = f.readlines()
        start_time = int(lines[0])
        for line in lines[1:]:
            tokens = line.strip().split('\t')
            crash_id = int(tokens[0])
            crash_time = int(tokens[1]);

            if crash_id not in all_bugs:
                unlisted.add(crash_id)
                continue
            if  crash_id not in crashes_map:
                crashes_map[crash_id] = crash_time
            else:
                if crashes_map[crash_id] > crash_time:
                    crashes_map[crash_id] = crash_time

    return start_time, crashes_map, unlisted

def read_all_bugs(all_bugs_path):
    all_bugs = set()
    with open(all_bugs_path) as f:
        for line in f.readlines():
            all_bugs.add(int(line.strip()))
    return all_bugs


def run(fuzzer, tool):
    cur_dir = os.path.dirname(os.path.abspath(__file__))
    all_bugs_path = os.path.join(cur_dir, 'subjects', 'lava_corpus', 'LAVA-M', tool, 'validated_bugs')
    output = os.path.join(cur_dir, '{}-{}-output.log'.format(fuzzer, tool))
    all_bugs = read_all_bugs(all_bugs_path)
    start_time, crashes, unlisted = get_crashes(all_bugs, output)

    step  = 60 * 10
    start = start_time
    end   = start_time + 60 * 60 * 5

    print('  ground: {} {}'.format(len(all_bugs), sorted(list(all_bugs))))
    print('unlisted: {} {}'.format(len(unlisted), sorted(list(unlisted))))
    print('  listed: {} {}'.format(len(crashes),  sorted(list(crashes.keys()))))

    steps = 0
    while start <= end:
        c = len([cid for cid,ctime in crashes.iteritems() if ctime <= start])
        print('{0}\t{1}'.format(steps,c))
        start = start + step
        steps = steps + 1


if __name__ == '__main__':
    try:
        run(sys.argv[1], sys.argv[2])
    except:
        traceback.print_exc()

