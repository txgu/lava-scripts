#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

TTIME=${TTIME:-5h}

export AFL_NO_UI=1

export AFL_HOME=${HOME}/Projects/aflfast
export FUZZER=aflfast
export AFL_OPTS="-m 100 -d -p fast"

export TOOL=md5sum
export OPTS=-c
nohup timeout -s 9 $TTIME bash ${DIR}/run-lava.sh > /dev/null 2>&1 &

export TOOL=base64
export OPTS=-d
nohup timeout -s 9 $TTIME bash ${DIR}/run-lava.sh > /dev/null 2>&1 &

export TOOL=uniq
export OPTS=
nohup timeout -s 9 $TTIME bash ${DIR}/run-lava.sh > /dev/null 2>&1 &

export TOOL=who
export OPTS=
nohup timeout -s 9 $TTIME bash ${DIR}/run-lava.sh > /dev/null 2>&1 &

