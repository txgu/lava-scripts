#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

AFL_HOME=${AFL_HOME:-${DIR}/../afl-2.52b}
AFL_OPTS=${AFL_OPTS:-"-d"}
FUZZER=${FUZZER:-afl}
LAVA_HOME=${DIR}/subjects/lava_corpus

TOOL=${TOOL:-uniq}
PREFIX=${PREFIX:-${DIR}/${FUZZER}-lava-bin/${TOOL}}
#PREFIX=${PREFIX:-${DIR}/qsym-lava-bin/${TOOL}}
TOOL_PATH=${PREFIX}/bin/${TOOL}

INPUT_DIR=${LAVA_HOME}/LAVA-M/${TOOL}/fuzzer_input
OUTPUT_DIR=${DIR}/${FUZZER}-output-${TOOL}

REF_DIR=${LAVA_HOME}/LAVA-M/${TOOL}/inputs
REF_LIST=${DIR}/${FUZZER}-${TOOL}-ref.log
cat ${LAVA_HOME}/LAVA-M/${TOOL}/validated_bugs | sort -u > $REF_LIST

VAL_LIST=${DIR}/${FUZZER}-${TOOL}-output.log
REF_VAL_LIST=${DIR}/${FUZZER}-${TOOL}-ref-output.log

SCRIPT_LOG=${DIR}/${FUZZER}-${TOOL}-script

script    -c "find $OUTPUT_DIR -path \"*/crashes/id*\" -exec /usr/bin/timeout -s 9 1s ${TOOL_PATH} ${OPTS} {} \\; ; exit" $SCRIPT_LOG > /dev/null
script -a -c "find $OUTPUT_DIR -path \"*/hangs/id*\" -exec /usr/bin/timeout -s 9 1s ${TOOL_PATH} ${OPTS} {} \\; ; exit" $SCRIPT_LOG > /dev/null
echo "Save test output to $VAL_LIST"
${DIR}/count-lava.sh ${SCRIPT_LOG} > $VAL_LIST

echo ">>> Compare test output vs ref list"
diff $VAL_LIST $REF_LIST
wc -l $VAL_LIST
wc -l $REF_LIST
exit

rm $SCRIPT_LOG
for BUGID in $(cat ${REF_ORIG_LIST})
do
    script -a -c "find $REF_DIR -name \"*${BUGID}*\" -exec /usr/bin/timeout -s 9 1s ${TOOL_PATH} ${OPTS} {} \\; ; exit" $SCRIPT_LOG > /dev/null
done
echo "Save ref output to $REF_VAL_LIST"
${DIR}/count-lava.sh ${SCRIPT_LOG} > $REF_VAL_LIST

echo ">>> Compare ref output vs ref list"
diff $REF_VAL_LIST $REF_LIST

