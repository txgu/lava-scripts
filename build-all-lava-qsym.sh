#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

export FUZZER=afl
export CC=${HOME}/Projects/afl-2.52b-origin/afl-clang-fast
export CXX=${HOME}/Projects/afl-2.52b-origin/afl-clang-fast++

# Optimization may remove some bugs
export AFL_DONT_OPTIMIZE=1
export CFLAGS="-g -finline-functions -funroll-loops"

TOOL=uniq       bash    ${DIR}/build-lava.sh
TOOL=base64     bash    ${DIR}/build-lava.sh
TOOL=who        bash    ${DIR}/build-lava.sh
TOOL=md5sum     bash    ${DIR}/build-lava.sh

export FUZZER=qsym
export CC=clang
export CXX=clang++
TOOL=uniq       bash    ${DIR}/build-lava.sh
TOOL=base64     bash    ${DIR}/build-lava.sh
TOOL=who        bash    ${DIR}/build-lava.sh
TOOL=md5sum     bash    ${DIR}/build-lava.sh

